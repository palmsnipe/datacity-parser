///<reference path='./definitions/node.d.ts' />

var fs = require('fs');
var csv = require('csv');

module DataCityParser {
	
	export class ParserCSV {
		constructor(public fileName: string) {
			if(fs.existsSync(fileName)) {
				console.log("le fichier existe");
				csv()
					.from.path(fileName, { delimiter: ',', escape: '"' })
					.transform( function(row){
					  row.unshift(row.pop());
					  return row;
					})
					.on('record', function(row,index){
					  console.log('#'+index+' '+JSON.stringify(row));
					})
					.on('close', function(count){
						console.log('Number of lines: '+count);
					});
			}
			else {
				console.log("le fichier n'existe pas");
			}
		}
	}
}

exports.parser = DataCityParser;